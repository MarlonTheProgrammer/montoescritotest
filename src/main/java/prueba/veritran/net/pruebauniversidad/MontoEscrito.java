package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;

public class MontoEscrito {
    private static Map<Integer,String> parseoNumeros = new HashMap<Integer,String>();
    public static void main(String[] args) {
        MontoEscrito.getMontoEscrito(0);
    }

    public static String getMontoEscrito(Integer valor){
        String resultado = new String();


        crearParseNumeros();
        if(parseoNumeros.containsKey(valor)){
            return  parseoNumeros.get(valor);

        }
        else
        {
            if(valor/10==0)
            {
                int Unidad = valor %10;
                resultado = parseoNumeros.get(Unidad);
            }

            if(valor/10==1)
            {
                resultado = parseoNumeros.get(valor);
            }

            //Todos los valores en decenas
            if ((valor/10>1) && (valor/10<10))
            {
                int Decena = valor / 10;
                if(Decena == 2)
                {
                    valor = valor %10;
                    resultado = "veinti"+ getMontoEscrito(valor);
                }
                else
                {
                    if (Decena >2)
                    {
                        Decena = Decena * 10;
                        valor = valor %10;
                        resultado = getMontoEscrito(Decena)+" "+"y"+" "+getMontoEscrito(valor);

                    }
                }
            }

            //Todos los valores en Centenas
            if ((valor/10>=10) && (valor/10<100)) {
                int Centenas = valor / 100;
                if (Centenas == 1) {
                    valor = valor % 100;
                    resultado = "ciento" + " " + getMontoEscrito(valor);

                } else {
                    if (Centenas > 1) {
                        Centenas = Centenas * 100;
                        valor = valor % 100;
                        resultado = parseoNumeros.get(Centenas) + " " + getMontoEscrito(valor);
                    }
                }
            }

            //Todos los valores en miles
                if ((valor/10>=100) && (valor/10<100000))
                {
                    int Milesimas= valor/1000;
                    if(Milesimas == 1)
                    {
                        valor = valor %1000;
                        resultado = "mil"+" "+getMontoEscrito(valor);

                    }

                    else
                    {   valor = valor%1000;

                        if ((Milesimas>1) && (valor!=0))
                        {

                            resultado = getMontoEscrito(Milesimas)+" "+"mil"+" "+getMontoEscrito(valor);
                        }
                        else resultado = getMontoEscrito(Milesimas)+" "+"mil";
                    }
            }

                //Todos los valores en millones
            if ((valor/10>=100000) && (valor/10<1000000000))
            {
                int Millones= valor/1000000;
                if(Millones == 1)
                {
                    valor = valor %1000000;
                    resultado = "millon"+" "+getMontoEscrito(valor);

                }

                else
                {   valor = valor%1000000;

                    if ((Millones>1) && (valor!=0))
                    {

                        resultado = getMontoEscrito(Millones)+" "+"millones"+" "+getMontoEscrito(valor);
                    }
                    else resultado = getMontoEscrito(Millones)+" "+"millones";
                }
            }

        }

return resultado;
    }

    private static void crearParseNumeros() {
        parseoNumeros.put(0,"cero");
        parseoNumeros.put(1,"uno");
        parseoNumeros.put(2,"dos");
        parseoNumeros.put(3,"tres");
        parseoNumeros.put(4,"cuatro");
        parseoNumeros.put(5,"cinco");
        parseoNumeros.put(6,"seis");
        parseoNumeros.put(7,"siete");
        parseoNumeros.put(8,"ocho");
        parseoNumeros.put(9,"nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(16, "dieciseis");
        parseoNumeros.put(17, "diecisiete");
        parseoNumeros.put(18, "dieciocho");
        parseoNumeros.put(19, "diecinueve");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(200, "doscientos");
        parseoNumeros.put(300, "trescientos");
        parseoNumeros.put(400, "cuatrocientos");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(600, "seiscientos");
        parseoNumeros.put(700, "setecientos");
        parseoNumeros.put(800, "ochosientos");
        parseoNumeros.put(900, "novecientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");
        parseoNumeros.put(1000000000, "un billon");
    }
}

// MR - Marlon Restrepo 1225093621   UTP-SEDE CUBA
